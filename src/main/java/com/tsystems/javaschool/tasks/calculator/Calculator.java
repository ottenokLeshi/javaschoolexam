package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;

public class Calculator {

    private static ArrayDeque<Double> numbersStack;
    private static ArrayDeque<Character> symbolsStack;

    /**
     * Checks the operator
     * @param operation - Character value
     * @return boolean value
     */
    private boolean isOperation(Character operation) {
        return operation.equals('+') || operation.equals('-') ||
                operation.equals('*') || operation.equals('/');
    }

    /**
     * Returns the priority of operator
     * @param operation - Character value of binary operation
     * @return int value containing the priority of operator
     */
    private int getPriority(Character operation) {
        return operation == '+' || operation == '-' ? 1 :
                operation == '/' || operation == '*' ? 2 : -1;
    }

    /**
     *
     * @param second - number
     * @param first - number
     * @param operation - operation that should be applied
     * @return double value containing result of applying the operation to two numbers
     */
    private Double calculate(Double second, Double first, Character operation) {
        Double result = null;
        switch (operation) {
            case '+':
                result = first + second;
                break;
            case '-':
                result = first - second;
                break;
            case '*':
                result = first * second;
                break;
            case '/':
                result = (second == 0 ? null : first / second);
                break;
        }
        return result;
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        numbersStack = new ArrayDeque<>();
        symbolsStack = new ArrayDeque<>();
        if (statement == null || statement == "") return null;
        Double result;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                symbolsStack.push(statement.charAt(i));
            } else if (statement.charAt(i) == ')') {
                while (!symbolsStack.peek().equals('(')) {
                    if (numbersStack.size() < 2) return null;
                    result = calculate(numbersStack.pop(), numbersStack.pop(), symbolsStack.pop());
                    if (result == null) return null;
                    numbersStack.push(result);
                }
                symbolsStack.pop();
            } else if (isOperation(statement.charAt(i))) {
                Character currentOperation = statement.charAt(i);
                while (numbersStack.size() >= 2 && !symbolsStack.isEmpty()
                        && getPriority(currentOperation) <= getPriority(symbolsStack.peek())) {
                    if (numbersStack.size() < 2) {
                        return null;
                    }
                    result = calculate(numbersStack.pop(), numbersStack.pop(), symbolsStack.pop());
                    if (result == null) {
                        return null;
                    }
                    numbersStack.push(result);
                }
                symbolsStack.push(currentOperation);
            } else {
                StringBuilder numberStr = new StringBuilder();
                while (i < statement.length() && !isOperation(statement.charAt(i)) &&
                        statement.charAt(i) != '(' &&
                        statement.charAt(i) != ')') {
                    numberStr.append(statement.charAt(i++));
                }
                i -= 1;
                try {
                    result = Double.parseDouble(numberStr.toString());
                } catch (Exception e) {
                    return null;
                }
                numbersStack.push(result);
            }
        }

        while (numbersStack.size() >= 2) {
            result = calculate(numbersStack.pop(), numbersStack.pop(), symbolsStack.pop());
            if (result == null) {
                return null;
            }
            numbersStack.push(result);
        }
        result = numbersStack.pop();
        if (symbolsStack.size() != 0 || numbersStack.size() != 0) return null;
        if (result % 1 == 0) {
            Integer intResult = result.intValue();
            return intResult.toString();
        }
        DecimalFormat df = new DecimalFormat("#.####");
        return df.format(result);
    }
}
