package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Objects;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int height = 0;
        int width;
        int k = 1;
        int size = inputNumbers.size();
        int bound = inputNumbers.size();

        inputNumbers.stream().filter(Objects::isNull).forEach(inputNumber -> {
            throw new CannotBuildPyramidException();
        });

        while (size > 0) {
            size -= k;
            k += 1;
            height += 1;
        }
        width = height * 2 - 1;

        if (size != 0){
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort((Integer o1, Integer o2) -> o1 - o2);

        for (int i = 0; i < inputNumbers.size() - 1; i++) {
            if (inputNumbers.get(i).equals(inputNumbers.get(i + 1))){
                throw new CannotBuildPyramidException();
            }
        }

        int[][] result = new int[height][width];
        int arrayIterator = 0;

        for (int i = 0; i < height; i++) {
            int firstIndex = height - i - 1;
            for (int j = 0; j <= i; j += 1, firstIndex += 2) {
                result[i][firstIndex] = inputNumbers.get(arrayIterator++);
            }
        }

        return result;
    }
}
